# Oomod

#### Easy-to-use frontend for modifying Oom modules.

[GitLab Project](https://gitlab.com/loopdotcoop/oomod/)  
[Online Demo](https://loopdotcoop.gitlab.io/oomod/)
