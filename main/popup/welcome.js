//// A popup which shows a welcome message.




//// DEPENDENCIES

import Popup from '../class/popup.js'




//// CLASS AND SINGLETON

class Welcome extends Popup {

    //// Same as a Popup template-string, with conditional ‘close’ button.
    get vueTemplate () { let innerHTML; return innerHTML = `
    <div
      class="${this.vueTagName}"
      v-bind:class="{ show:'${this.tagName}' === Router.popupAction }">
      <button
        v-if="Router.coreAction || Router.gridAction"
        v-on:click="closePopup" class="close" title="Close">&times;</button>
      ${this.vueTemplateInner}
    </div>
    `}

    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Welcome</h1>
    <a href="#list-users">List Users</a>
    `}

}

//// Create the singleton instance of this module.
new Welcome()
