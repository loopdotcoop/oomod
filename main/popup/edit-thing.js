//// Lets the user view and edit a Thing.




//// DEPENDENCIES

import Popup from '../class/popup.js'
import hub from '../core/hub.js'




//// CLASS AND SINGLETON

class EditThing extends Popup {

    //// Returns an object containing this module’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            deleteItem () { hub.fire('thing-delete', this.item.id) }
        })
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Edit Thing</h1>
    <div v-if="'edit-thing' === Router.popupAction">
      <oomod-thing-form
        v-if="ThingList.items.find( el => Router.popupArgs[0] == el.id )"
        v-bind:item="ThingList.items.find( el => Router.popupArgs[0] == el.id )">
      </oomod-thing-form>
    <div v-else>No such thing #{{ Router.popupArgs[0] }}</div>
    </div>
    <div v-else>Not editing a thing</div>
    <a href="#list-things">List Things</a>
    `}

}

//// Create the singleton instance of this module.
new EditThing()
