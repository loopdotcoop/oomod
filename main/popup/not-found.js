//// A popup which appears when the current route is not recognised.




//// DEPENDENCIES

import Popup from '../class/popup.js'




//// CLASS AND SINGLETON

class NotFound extends Popup {

    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Not Found</h1>
    `}

}

//// Create the singleton instance of this module.
new NotFound()
