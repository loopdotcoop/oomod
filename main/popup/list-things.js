//// Lets the user view and edit a list of Thing items.




//// DEPENDENCIES

import Popup from '../class/popup.js'
import hub from '../core/hub.js'




//// CLASS AND SINGLETON

class ListThings extends Popup {

    //// Returns an object containing this module’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            addItem () { hub.fire('thing-add') }
        })
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Things</h1>
    <button v-on:click="addItem()">Add Thing</button>
    <oomod-thing-list></oomod-thing-list>
    `}

}

//// Create the singleton instance of this module.
new ListThings()
