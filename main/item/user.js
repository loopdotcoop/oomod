//// An Oomod user, which roughly equates to a GitLab user.




//// DEPENDENCIES

import Item from '../class/item.js'
import config from '../core/config.js'
import hub from '../core/hub.js'
const IS_EDITABLE = 'IS_EDITABLE'
const IS_ON_FORM = 'IS_ON_FORM'




//// RECORD API

hub.on('all-modules-booted', e => {

    //// Create this item’s shared config object and record its `api`.
    if (! config.User || ! config.User.api)
        hub.fire('config-set', User.name, 'api', User.api)

})




//// CLASS

export default class User extends Item {




    //// API

    static get api () {
        return Object.assign({}, super.api, {
            isInvalid: true // a newly created user has not password hashes
          , oomodUsername: {
                default: ''
              , IS_EDITABLE, IS_ON_FORM
            }
          , oomodPasswordHash: {
                default: ''
            }
          , gitlabUsername: {
                default: ''
              , IS_EDITABLE, IS_ON_FORM
            }
          , gitlabPassword: {
                default: ''
              , IS_EDITABLE
            }
        })
    }




    //// CONSTRUCTOR

    constructor (custom={}) {
        super(custom)

        //// Record custom instantiation values, or fall back to defaults.
        const { oomodUsername, oomodPasswordHash, gitlabUsername, gitlabPassword } = custom
        const api = this.constructor.api
        this.oomodUsername     = oomodUsername     || api.oomodUsername.default
        this.oomodPasswordHash = oomodPasswordHash || api.oomodPasswordHash.default
        this.gitlabUsername    = gitlabUsername    || api.gitlabUsername.default
        this.gitlabPassword    = gitlabPassword    || api.gitlabPassword.default
    }




    //// STATIC METHODS

    //// Validates each api property and then validates the instance as a whole.
    static validate (item) {
        if (! item.title || ! item.oomodUsername || ! item.gitlabUsername)
            return false
        return true
    }

}
