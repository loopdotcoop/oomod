//// Usage example - how to define a typical row.




//// DEPENDENCIES

import Row from '../class/row.js'
import hub from '../core/hub.js'




//// CLASS

export default class ThingRow extends Row {




    //// VUE

    //// Returns an object containing this row’s Vue methods.
    //// Note that the 'thing-*' events are listened for by item/thing-list.js.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            toggleFlag () {
                hub.fire(this.item.tagName+'-edit', this.item.id, 'flag', ! this.item.flag)
            }
        })
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () {
        let innerHTML = `
        <button v-on:click="toggleFlag">Toggle</button>
        <span v-if="item.flag">yep</span>
        <span v-else>nope</span>`
        return super.vueTemplateInner + innerHTML
    }

}

//// Create the singleton instance of this row.
new ThingRow()
