//// A module which maintains and renders a list of Thing items.




//// DEPENDENCIES

import List from '../class/list.js'
import Thing from './thing.js' // for itemClass, but also to record its API
import ThingRow from './thing-row.js' // for rowClass




//// CLASS

class ThingList extends List {

    //// An instance of this class will be passed to 'state-add'.
    get itemClass () { return Thing }

    //// Useful for building vueTemplateInner.
    get rowClass () { return ThingRow }

}

//// Create the singleton instance of this module.
new ThingList()
