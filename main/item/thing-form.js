//// A module which maintains and renders a form for editing a Thing item.




//// DEPENDENCIES

import Form from '../class/form.js'




//// CLASS

class ThingForm extends Form { }

//// Create the singleton instance of this module.
new ThingForm()
