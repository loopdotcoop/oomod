//// Provides client-side persistance of some of the app’s state.
//// #persist/ shows a list of all localStorage, and has tools for copying
//// persisted state in and out of localStorage.




//// DEPENDENCIES

import Core from '../class/core.js'
import config from './config.js'
import hub from './hub.js'
import state from './state.js'




//// CLASS AND SINGLETON

class Persist extends Core {

    //// `invalidJsonMerge` keeps the ‘Merge JSON’ button disabled when the
    //// <textarea> does not contain a valid JSON object.
    get vueComputed () {
        return Object.assign({}, super.vueComputed, {
            invalidJsonMerge: function (parsed) {
                try { parsed = JSON.parse((state.Persist.jsonMerge||'').trim())
                } catch (err) { return true }
                if ('object' !== typeof parsed) return true
            }
        })
    }

    //// See below for the method bodies.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {

            //// Copy localStorage to the clipboard as a JSON string.
            storageToJSON
          , onCopySuccess: function () {
                hub.fire('state-moment', 'Persist', 'copySuccess', true ) }
          , onCopyError: function () {
                hub.fire('state-moment', 'Persist', 'copyError', true ) }

            //// If the JSON is valid and an object, merge it into localStorage.
          , revealJsonMerge
          , runJsonMerge

            //// Show an editable list of all localStorage items.
          , summarizeLocalStorage
          , readLocalStorage
          , deleteLocalStorage
        })
    }

    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Persist</h1>

    <button title="Copy localStorage to the clipboard as a JSON string"
      v-clipboard:copy="storageToJSON()"
      v-clipboard:success="onCopySuccess"
      v-clipboard:error="onCopyError"
      v-bind:class="{ success:Persist.copySuccess, error:Persist.copyError }"
    >Copy Storage</button>

    <button title="If the JSON is valid and an object, merge it into localStorage"
      v-if="Persist.jsonMergeIsRevealed"
      v-on:click="runJsonMerge()"
      v-bind:disabled="invalidJsonMerge"
      v-bind:class="{ success:Persist.mergeSuccess, error:Persist.mergeError }"
    >Run Merge</button>
    <button title="Input a JSON object to merge into localStorage"
      v-else
      v-on:click="revealJsonMerge()"
      v-bind:class="{ success:Persist.mergeSuccess, error:Persist.mergeError }"
    >Merge JSON</button>
    <div
      v-bind:class="{ show:Persist.jsonMergeIsRevealed }"
      class="oomod-persist-json-input">
      <textarea placeholder="Type or paste a JSON object here"
        v-model="Persist.jsonMerge"></textarea>
    </div>

    <hr>

    <div v-for="meta in Persist.meta">
      <button v-on:click="deleteLocalStorage(meta.id)">Delete</button>
      <button v-on:click="readLocalStorage(meta.id)">Read</button>
      <tt style="display:inline-block">{{ meta.id }}:<br>
      {{ summarizeLocalStorage(meta.id) }}</tt><br>
      <pre v-if="meta.val">{{ meta.val }}</pre>
    </div>
    `}

    constructor () {
        super()

        //// Record all localStorage keys, even ones nothing to do with Oomod.
        //// Used to keep the #persist/ list up to date.
        hub.on('persist-storage-updated', evt =>
            hub.fire('state-set', 'Persist', 'meta'
              , Object.keys(localStorage).map( id => ({ id, val:null }))))
        hub.fire('persist-storage-updated')

        //// Load localStorage data into state during the boot lifecycle.
        hub.on('persist-load-storage-into-state'
          , this.loadStorageIntoState.bind(this) )
        hub.on('all-modules-booted', evt =>
            hub.fire('persist-load-storage-into-state') )

        //// Update localStorage every time state changes.
        hub.on('state-updated', this.updateStorage.bind(this) )
    }

    //// Records current state into localStorage. Triggered when state changes.
    updateStorage (evtName, origEvtName, ns, key, ...args) {
        if (! config[ns] || ! config[ns].persist) return // module not persisted
        if (0 > config[ns].persist.indexOf(key) ) return // key not persisted
        localStorage.setItem(
            `oomod.${ns}.${key}`
          , JSON.stringify(state[ns][key])
        )
        hub.fire('persist-storage-updated')
    }

    //// Loads state from localStorage into the app’s `state` object. Triggered
    //// by boot.js at an early stage in the initial lifecycles. Also called
    //// directly after a JSON merge.
    loadStorageIntoState () {
        for (const ns in config)
            if (config[ns].persist)
                config[ns].persist.map( key => {
                    const val = localStorage.getItem(`oomod.${ns}.${key}`)
                    if (val) state[ns][key] = JSON.parse(val)
                })
    }

}

//// Create the singleton instance of this module.
new Persist()




//// VUE METHODS

//// Converts the whole of localStorage into a JSON string. Used by the
//// ‘Copy Storage’ button.
function storageToJSON () {
    const out = {}
    Object.keys(localStorage).map( id => {
        const raw = localStorage.getItem(id)
        if ('string' !== typeof raw) throw Error(`'${id}' is type ${typeof raw}`)
        let parsed
        try { parsed = JSON.parse(raw) } catch (err) { return out[id] = raw }
        out[id] = 'string' === typeof parsed ? `"${parsed}"` : parsed
    })
    return JSON.stringify(out)
}

//// Reveals the <textarea> for inputting a JSON object.
function revealJsonMerge () {
    hub.fire('state-set', 'Persist', 'jsonMergeIsRevealed', true)
}

//// Shallow-merges a JSON object into localStorage.
function runJsonMerge (parsed) {
    try { parsed = JSON.parse( (state.Persist.jsonMerge || '').trim() )
    } catch (err) { return }
    if ('object' !== typeof parsed) return
    for (const key in parsed)
        localStorage.setItem( key, JSON.stringify(parsed[key]) )
    hub.fire('persist-storage-updated')
    hub.fire('persist-load-storage-into-state')
    hub.fire('state-set', 'Persist', 'jsonMergeIsRevealed', false)
    hub.fire('state-moment', 'Persist', 'mergeSuccess', true )
    // hub.fire('state-moment', 'Persist', 'mergeError', true ) } //@TODO
}

//// Returns a brief summary of a localStorage item.
function summarizeLocalStorage (id) {
    const raw = localStorage.getItem(id) // expect a string
    if ('string' !== typeof raw) throw Error(`'${id}' is type ${typeof raw}`)
    let parsed
    try { parsed = JSON.parse(raw) } catch (err) { // not JSON
        if (29 > raw.length) return `'${ raw }'` // short string
        return `'${ raw.slice(0,25) }...'` // truncate a long string
    }
    if (null == parsed) return `null`
    if ('string' === typeof parsed)
        if (29 > parsed.length) return `"${ parsed }"`
        else return `"${ parsed.slice(0,25) }..."`
    if ( Array.isArray(parsed) )
        return `[ ... ${ parsed.length
            } element${ 1 === parsed.length ? '' : 's' } ... ]`
    if ('object' === typeof parsed)
        return `{ ... ${ Object.keys(parsed).length
            } propert${ 1 === Object.keys(parsed).length ? 'y' : 'ies' } ... }`
    return `${ typeof parsed } ${ parsed }`
}

//// Returns a localStorage item’s value. Pretty-formats it, if JSON.
function readLocalStorage (id) {
    const raw = localStorage.getItem(id)
    let val
    try { val = JSON.stringify( JSON.parse(raw), null, '  ' ) } catch (err) { }
    hub.fire('state-edit', 'Persist', 'meta', id, 'val', val || raw)
}

//// Deletes a localStorage item, even an item nothing to do with Oomod.
function deleteLocalStorage (id) {
    localStorage.removeItem(id)
    hub.fire('persist-storage-updated')
}
