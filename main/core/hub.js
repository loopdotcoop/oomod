//// Receives and broadcasts events. Helps keep things loosely coupled.




//// CLASS AND SINGLETON

class Hub {

    ////
    constructor () {

        //// Define `handlerArrays` - an object where each key is an event-name,
        //// and each value is an array of handlers.
        this.handlerArrays = {}

        //// Hook up boot.js events to module lifecycle methods.
        this.on('boot-all-modules', this.bootModule.bind(this))
        this.on('define-all-vue-components', this.defineVueComponent.bind(this))
        this.on('append-all-vue-instances', this.appendVueInstance.bind(this))

        //// Create the Hub’s shared state object and record initial values.
        this.fire('state-set', 'Hub', 'lifecycle', 'module-instantiated')
        this.fire('state-set', 'Hub', 'mounted', 'not-yet')
    }

    //// Registers a new handler, under one or more event-names. Similar to
    //// the DOM’s `addEventListener()`.
    on (eventName, handler) {
        const parts = eventName.split(' ')
        if (1 < parts.length) { // go recursive for space-delimited event-names
            parts.map( part => this.on(part, handler) )
        } else { // create a handler-array if none currently exists
            this.handlerArrays[eventName] = this.handlerArrays[eventName] || []
            this.handlerArrays[eventName].push(handler)
        }
    }

    //// Triggers the handlers for a given event-name. The event-name, along
    //// with all arguments, will be passed as arguments to the handler.
    //// Unusually for an event system, fire() returns an array containing the
    //// results of calling each handler.
    fire (eventName, ...payload) {
        const handlerArray = this.handlerArrays[eventName]
        if (! handlerArray) return // no such handler-array
        return handlerArray.map(
            handler => handler.apply( null, [eventName].concat(payload) )
        )
    }

    //// See class/module.js. Note that Hub tries to act like a Core module, but
    //// does not actually use Vue.
    bootModule (evt) {
        this.fire('config-set', 'Hub', 'persist', [])
        this.fire('state-set', 'Hub', 'lifecycle', 'module-booted')
        this.fire('module-booted', 'Hub')
    }
    defineVueComponent (evt) {
        this.fire('state-set', 'Hub', 'lifecycle', 'vue-component-defined')
        this.fire('vue-component-defined', 'Hub')
    }
    appendVueInstance (evt) {
        this.fire('state-set', 'Hub', 'lifecycle', 'vue-instance-appended')
        this.fire('vue-instance-appended', 'Hub')
        this.fire('state-set', 'Hub', 'mounted', 'ok')
        this.fire('vue-instance-mounted', 'Hub')
    }
}

//// Create the singleton Hub instance.
export default new Hub()
