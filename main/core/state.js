//// Shared application state.

//// Every module gets a property, eg `state.Config` or `state.CodeEditor`, and
//// each property is an object containing at least the `lifecycle` string.
//// It’s best practice to modify state by firing a 'state-set' event (or
//// 'state-add', 'state-delete', etc).




//// DEPENDENCIES

import Core from '../class/core.js'
import hub from './hub.js'




//// CLASS AND SINGLETON

class State extends Core {
    constructor () {
        super()

        //// Record a new value, or change an existing value.
        hub.on('state-set', (evtName, ns, key, val) => {
            this[ns] = this[ns] || {} // create the namespace, if missing
            if (this[ns][key] === val) return // is already that value
            Vue.set(this[ns], key, val) // Vue.set() makes a new key reactive
            hub.fire('state-updated', evtName, ns, key, val)
        })

        //// Same as 'state-set' but reverts to its previous value after 1200ms.
        hub.on('state-moment', (evtName, ns, key, val) => {
            const prev = this[ns][key]
            hub.fire('state-set', ns, key, val)
            setTimeout( () => hub.fire('state-set', ns, key, prev), 1200 )
            //@TODO prevent getting stuck after lots of rapid calls
        })

        //// The following event handlers perform BREAD operations on an array.
        hub.on('state-add', (evtName, ns, key, val) => {
            this[ns] = this[ns] || {}
            this[ns][key] = this[ns][key] || [] // create the array if missing
            this[ns][key].push(val)
            hub.fire('state-updated', evtName, ns, key, val)
            if (val.id) return val.id // useful when creating a new item
        })
        hub.on('state-edit', (evtName, ns, key, id, prop, val) => {
            if (! this[ns] || ! this[ns][key]) return // no such array
            const item = this[ns][key].find( el => id === el.id )
            if (! item) return // not found
            item[prop] = val // (though it will sometimes already be that value)
            hub.fire('state-updated', evtName, ns, key, id, prop, val)
        })
        hub.on('state-delete', (evtName, ns, key, id) => {
            if (! this[ns] || ! this[ns][key]) return
            const index = this[ns][key].findIndex( el => id === el.id )
            if (0 > index) return // not found
            this[ns][key].splice(index, 1)
            hub.fire('state-updated', evtName, ns, key, id)
        })

        //// The `super()` call above wont have initialised State’s own state.
        hub.fire('state-set', 'State', 'lifecycle', 'module-instantiated')
        hub.fire('state-set', 'State', 'mounted', 'not-yet')
        hub.fire('state-set', 'State', 'singleton', this)
    }
}

//// Create the singleton instance of this module.
export default new State()
