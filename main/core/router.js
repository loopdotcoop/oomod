//// Xx.




//// DEPENDENCIES

import Core from '../class/core.js'
import hub from './hub.js'
import state from './state.js'

//// Used for `... instanceof Popup`, etc.
import Grid from '../class/grid.js'
import Popup from '../class/popup.js'



//// CLASS AND SINGLETON

class Router extends Core {
    constructor () {
        super()

        //// Instantiate shared state properties, ready for Vue to observe.
        hub.fire('state-set', 'Router', 'coreFrontmost', null)
        hub.fire('state-set', 'Router', 'coreAction', null)
        hub.fire('state-set', 'Router', 'gridAction', null)
        hub.fire('state-set', 'Router', 'popupAction', null)
        hub.fire('state-set', 'Router', 'coreArgs', [])
        hub.fire('state-set', 'Router', 'gridArgs', [])
        hub.fire('state-set', 'Router', 'popupArgs', [])

        //// Listen for an instruction to change the URL hash.
        hub.on( 'router-update-hash', (e, h) => window.location.hash = h )

        //// Newly routed-to core modules go in front of popups, and vice versa.
        hub.on( 'state-set', (evtName, ns, key, val) => {
            if ('Router' !== ns || null === val) return
            if ('coreAction' === key)
                hub.fire('state-set', 'Router', 'coreFrontmost', true)
            else if ('popupAction' === key)
                hub.fire('state-set', 'Router', 'coreFrontmost', false)
        })

        //// Listen for an instruction to close the current core/popup module.
        hub.on( 'router-close-core', this.onCloseCore )
        hub.on( 'router-close-popup', this.onClosePopup )

        //// Listen for an instruction to maximize or minimize grid modules.
        hub.on( 'router-grid-fill-one', this.onGridFillOne )
        hub.on( 'router-grid-show-all', this.onGridShowAll )

        //// Convert the URL’s initial hash to a route, after boot is complete.
        hub.on('app-ready', this.onHashChange)

        //// Call onHashChange() when the window’s URL hash changes.
        window.addEventListener('hashchange', this.onHashChange, false)
    }

    onCloseCore () {
        const { gridAction, popupAction, gridArgs, popupArgs } = state.Router

        //// Reset the core route state.
        hub.fire('state-set', 'Router', 'coreAction', null)
        hub.fire('state-set', 'Router', 'coreArgs', [])

        //// Where a popup and a grid action were both visible before the core
        //// module was closed, navigate to the popup.
        if (popupAction)
            hub.fire('router-update-hash', `${popupAction}/${popupArgs.join('/')}`)

        //// Although '#project/123' will work, the alias '#/123' is preferred.
        else if ('project' === gridAction)
            hub.fire('router-update-hash', `/${gridArgs.join('/')}`)

        //// Failing the above, show the most recent grid action.
        else if (gridAction)
            hub.fire('router-update-hash', `${gridAction}/${gridArgs.join('/')}`)

        //// And as a final fallback, show the welcome message.
        else
            hub.fire('router-update-hash', '')
    }

    onClosePopup () {
        const { coreAction, gridAction, popupAction, coreArgs, gridArgs }
          = state.Router

        //// Don’t allow the screen to be blank, eg when the page first loads
        //// and no grid or core modules are visible.
        if ('welcome' === popupAction && ! coreAction && ! gridAction) return

        //// Reset the popup route state. Note that the core and grid route
        //// states are NOT reset - they will still be showing underneath.
        hub.fire('state-set', 'Router', 'popupAction', null)
        hub.fire('state-set', 'Router', 'popupArgs', [])

        //// Where a core and a grid action were both visible before the popup
        //// popped up, navigate to the core action.
        if (coreAction)
            hub.fire('router-update-hash', `${coreAction}/${coreArgs.join('/')}`)

        //// Although '#project/123' will work, the alias '#/123' is preferred.
        else if ('project' === gridAction)
            hub.fire('router-update-hash', `/${gridArgs.join('/')}`)

        //// Failing the above, show the most recent grid action.
        else if (gridAction)
            hub.fire('router-update-hash', `${gridAction}/${gridArgs.join('/')}`)

        //// And as a final fallback, show the welcome message.
        else
            hub.fire('router-update-hash', '')
    }

    onGridFillOne (evtName, tagName) {
        const { gridArgs } = state.Router
        hub.fire('router-update-hash', `${tagName}/${gridArgs.join('/')}`)
    }

    onGridShowAll () {
        const { gridArgs } = state.Router
        hub.fire('router-update-hash', `/${gridArgs.join('/')}`)
    }

    onHashChange () {

        //// Split the new hash into an action and an array of arguments.
        let args = window.location.hash.slice(1).split('/')

        //// The first part of the hash is the action. Often the action is
        //// equivalent to a module-name, eg 'edit-user' to 'EditUser'.
        const action = args.shift().toLowerCase()
        let name = action.replace( /-([a-z])/ig, m => m[1].toUpperCase() )
        if (name) name = name[0].toUpperCase() + name.slice(1)

        //// An empty-string action with no arguments shows the ‘Welcome’ popup.
        if ('' === action && 0 === args.length)
            hub.fire('state-set', 'Router', 'popupAction', 'welcome')

        //// An empty-string action with arguments is an alias of ‘project’.
        //// Note that there is no main/grid/project.js - this is a special
        //// action which displays many grid modules at once.
        else if ('' === action || 'project' === action) {
            hub.fire('state-set', 'Router', 'gridAction', 'project')
            hub.fire('state-set', 'Router', 'gridArgs', args)
        }

        //// Deal with an action which is not equivalent to a module-name.
        else if (! state[name])
            hub.fire('state-set', 'Router', 'popupAction', 'not-found')

        //// Show a core module - this is mostly useful for developers.
        //// Note that Hub is not available. @TODO how can Hub just extend Core?
        else if (state[name].singleton instanceof Core) {
            hub.fire('state-set', 'Router', 'coreAction', action)
            hub.fire('state-set', 'Router', 'coreArgs', args)
        }

        //// Fill the window with one of the grid modules.
        else if (state[name].singleton instanceof Grid) {
            hub.fire('state-set', 'Router', 'gridAction', action)
            hub.fire('state-set', 'Router', 'gridArgs', args)
        }

        //// Show a popup.
        else if (state[name].singleton instanceof Popup) {
            hub.fire('state-set', 'Router', 'popupAction', action)
            hub.fire('state-set', 'Router', 'popupArgs', args)
        }

        //// For any other modules, eg forms, show the 'Not Found' popup.
        else
            hub.fire('state-set', 'Router', 'popupAction', 'not-found')

        //// Tell any interested modules that the route has changed.
        hub.fire('router-hash-updated', action, ...args) //@TODO remove if unused
    }
}

//// Create the singleton instance of this module.
new Router()
