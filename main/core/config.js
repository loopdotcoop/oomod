//// Shared app configuration.




//// DEPENDENCIES

import Core from '../class/core.js'
import hub from './hub.js'




//// CLASS AND SINGLETON

class Config extends Core {
    constructor () {
        super()

        //// Record a new value. The values must not be changed after that!
        hub.on('config-set', (evtName, ns, key, val) => {
            this[ns] = this[ns] || {} // create the namespace, if missing
            if (null != this[ns][key])
                throw Error(`'${ns}.${key}' has already been 'config-set'`)
            this[ns][key] = val
            hub.fire('config-updated', evtName, ns, key, val) //@TODO remove if unused
        })

        this.dev = 2 // verbosity of debugging logs in the browser console //@TODO move to Dev module
    }
}

//// Create the singleton instance of this module.
export default new Config()
