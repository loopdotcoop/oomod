//// Loads modules, coordinates their initial lifecycles, and fires 'app-ready'.




//// TIMEOUT

//// If the app isn’t ready within ten seconds, give up.
const timeout = setTimeout( () => {
    for (const key in state) console.log(key, state[key].mounted)
    throw Error('App still not ready') }, 10000)
hub.on('app-ready', () => clearTimeout(timeout) )




//// MODULES

//// boot.js dependencies.
import hub    from './core/hub.js'
import state  from './core/state.js'
import config from './core/config.js'

//// Other core app modules.
import './core/persist.js'
import './core/router.js'

//// Grid.
import './grid/meta.js'
import './grid/preview.js'

//// Form, List and Row.
import './item/thing-form.js'
import './item/thing-list.js'
import './item/user-form.js'
import './item/user-list.js'

//// Popup.
import './popup/edit-thing.js'
import './popup/edit-user.js'
import './popup/list-things.js'
import './popup/list-users.js'
import './popup/not-found.js'
import './popup/welcome.js'




//// LIFECYCLE EVENTS

//// Each time a module completes one of its lifecycle-methods, check whether
//// all modules have completed that lifecycle.
hub.on('module-booted', onLifecycleUpdate)
hub.on('vue-component-defined', onLifecycleUpdate)
hub.on('vue-instance-appended', onLifecycleUpdate)
hub.on('vue-instance-mounted', onMountedUpdate)

//// When all modules have booted, define all their Vue components.
hub.on('all-modules-booted', e => hub.fire('define-all-vue-components'))

//// When all Vue components have been defined, append the instances.
hub.on('all-vue-components-defined', e => hub.fire('append-all-vue-instances'))

//// When all the instances have been appended, create the main Vue instance.
hub.on('all-vue-instances-appended', startVue)

//// When all instances have mounted, the app is ready.
hub.on('all-vue-instances-mounted', e => hub.fire('app-ready'))

//// Show a developer log when the app is ready.
hub.on('app-ready', e => { if (config.dev) console.log('App ready!') })

//// Tell every module to run its `bootModule()` method. Triggering this event
//// kicks off the whole shebang.
hub.fire('boot-all-modules')




//// LIFECYCLE EVENT HANDLERS

//// Handles events fired by modules when they complete a lifecycle-method.
//// If all modules have completed that lifecycle, it triggers the next stage.
function onLifecycleUpdate (eventName, moduleName) {
    const moduleState = state[moduleName]

    //// Do a little validation.
    if (null == moduleState)
        throw Error(`Unexpected module '${moduleName}'`)
    if (eventName !== moduleState.lifecycle) // eg, both are 'module-booted'
        throw Error(`'${moduleName}' has inconsistent lifecycle state`)

    //// If modules are at different lifecycle stages, do nothing.
    for (const key in state) if (eventName !== state[key].lifecycle) return

    //// Signal that this lifecycle-stage has been completed by all modules.
    const done =
        'module-booted'          === eventName ? 'all-modules-booted'
      : 'vue-component-defined'  === eventName ? 'all-vue-components-defined'
      : 'vue-instance-appended'  === eventName ? 'all-vue-instances-appended'
      : `eventName '${eventName}' not recognised`
    if (1 < config.dev) console.log(`'${done}' x${Object.keys(state).length}`)
    hub.fire(done)
}

//// Similar to `onLifecycleUpdate()`, but just handles 'vue-instance-mounted'.
function onMountedUpdate () {

    //// If any core/grid/popup module has not mounted, do nothing.
    let cgpTally = 0
    for (const key in state) {
        if (! state[key].mounted) continue // not a core/grid/popup module
        cgpTally++
        if ('ok' !== state[key].mounted) return // not all mounted yet
    }

    //// Signal that all core/grid/popup modules have mounted.
    const done = 'all-vue-instances-mounted'
    if (1 < config.dev) console.log(`'${done}' x${cgpTally}`)
    hub.fire(done)
}

//// Creates the main Vue instance, after 'all-vue-instances-appended' fires.
//// This leads to every module’s Vue instance firing 'vue-instance-mounted'.
function startVue () {
    new Vue({
        el: '#oomod-main'
      , data: function () { return state }
    })
}
