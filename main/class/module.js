//// Base class for everything which defines a Vue component - which is
//// everything except Items.




//// DEPENDENCIES

import hub from '../core/hub.js'
import state from '../core/state.js'




//// CLASS

export default class Module {

    //// Xx, eg 'FooBar' becomes 'foo-bar'.
    static get tagName () { return '' +
        this.name.replace(/([a-z])([A-Z])/g,'$1-$2').toLowerCase() }
    get tagName () { return this.constructor.tagName }




    //// VUE

    //// The element-name of this module’s Vue component, eg <oomod-foo-bar>.
    static get vueTagName () { return 'oomod-' + this.tagName }
    get vueTagName () { return 'oomod-' + this.tagName }

    //// HTML container for this module’s Vue instance. Null for Form/List/Row.
    get $vueWrap () { return null }

    //// Returns this module’s Vue component’s options.
    vueOptions () {

        //// All Vue components have a reference to the module-class which defined them.
        const computed = this.vueComputed
        const classRef = this.constructor
        computed.class = function () { return classRef }

        //// Xx.
        return {
            computed: computed
          , data: this.vueData
          , methods: this.vueMethods
          , mounted: this.vueMounted
          , props: this.vueProps
          , template: this.vueTemplate
        }
    }

    //// Returns the `computed` option for this module’s Vue component.
    get vueComputed () { return {} }

    //// A module’s `data` property is usually just the shared app state.
    get vueData () { return function () { return state } }

    //// Returns an object containing this module’s Vue methods.
    get vueMethods () { return {} }

    //// Returns this module’s Vue `mounted()` hook. Core, grid and popup
    //// modules set `mounted` state to 'ok' and fire 'vue-instance-mounted'.
    get vueMounted () { return function () {} }

    //// Returns this module’s Vue props.
    get vueProps () { return ['item'] }

    //// Returns this module’s Vue template-string.
    get vueTemplate () {
        return `<div class="${this.vueTagName}"><div>${this.vueTemplateInner}</div></div>`
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () {
        return this.constructor.name
    }




    //// CONFIG

    get persistList () { return [] } // don’t persist anything




    //// LIFECYCLE

    //// The constructor records the module’s name, inits its shared state, and
    //// sets up various event listeners.
    constructor () {

        //// Validate the module’s name (it should correspond to the filename).
        const name = this.constructor.name
        const nameRx = /^[A-Z][A-Za-z0-9]{1,31}$/
        if (! nameRx.test(name) )
            throw Error(`'${name}' fails ${nameRx}`)

        //// Ensure the module name is unique, and that only singletons exist.
        if (Module.names[name])
            throw Error(`'${name}' already exists`)
        Module.names[name] = true

        //// Hook up boot.js events to module lifecycle methods.
        hub.on('boot-all-modules', this.bootModule.bind(this) )
        hub.on('define-all-vue-components', this.defineVueComponent.bind(this) )
        hub.on('append-all-vue-instances', this.appendVueInstance.bind(this) )

        //// Create this module’s shared state object and record its `lifecycle`
        //// as 'module-instantiated'. Every module’s state object must be in
        //// place before any Vue components can be defined.
        hub.fire('state-set', name, 'lifecycle', 'module-instantiated')

        //// Record a reference to this instance in `state`.
        hub.fire('state-set', name, 'singleton', this)
    }

    //// Provides initial preparation of this instance after all modules have
    //// loaded, but before any Vue components have been defined or appended.
    bootModule (evt) {
        const [name, stage] = [this.constructor.name, 'module-booted']

        //// Create this module’s shared config object and record its `persist`.
        hub.fire('config-set', name, 'persist', this.persistList)

        //// ...
        //// Modules which override this method add their own activities here.
        //// ...

        //// Update `lifecycle` state. Signal that the module has booted.
        hub.fire('state-set', name, 'lifecycle', stage)
        hub.fire(stage, name)
    }

    //// Defines this module’s Vue component, after all modules have booted.
    //// At this point, `state` and `config` are fully populated.
    defineVueComponent (evt) {
        const [name, stage] = [this.constructor.name, 'vue-component-defined']

        //// Define this module’s Vue component, eg <oomod-foo-bar>.
        Vue.component( this.vueTagName, this.vueOptions.apply(this) )

        //// ...
        //// Modules which override this method add their own activities here.
        //// ...

        //// Update `lifecycle`. Signal that the Vue component has been defined.
        hub.fire('state-set', name, 'lifecycle', stage)
        hub.fire(stage, name)
    }

    //// Provides an opportunity to append an instance of this module’s Vue
    //// component to the DOM, after all the Vue components have been defined.
    appendVueInstance (evt) {
        const [name, stage] = [this.constructor.name, 'vue-instance-appended']

        //// For singleton modules, create the single Vue instance in the
        //// proper container. `$vueWrap` is null for other kinds of modules -
        //// they create and destroy multiple Vue instances as required.
        if (this.$vueWrap)
            this.$vueWrap.appendChild(document.createElement(this.vueTagName))

        //// ...
        //// Modules which override this method add their own activities here.
        //// ...

        //// Update `lifecycle`. Signal that the Vue instance has been appended,
        //// if it needed to be.
        hub.fire('state-set', name, 'lifecycle', stage)
        hub.fire(stage, name)
    }

}

//// A static lookup-object, which ensures that all module names are unique.
//@TODO also check List names
Module.names = {
    Module: true // prevent the `Module` class from being instantiated
}
