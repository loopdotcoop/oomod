//// Base class for the app’s core modules, eg core/config.js.




//// DEPENDENCIES

import Singleton from './singleton.js'
import hub from '../core/hub.js'




//// CLASS

export default class Core extends Singleton {

    //// All core modules have a closeCore() method.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            closeCore () { hub.fire('router-close-core') }
        })
    }

    //// See main/class/singleton.js for details.
    get $vueWrap () { return document.querySelector('#oomod-core') }

    //// Same as a Module template-string, with a ‘close’ button.
    get vueTemplate () { let innerHTML; return innerHTML = `
    <div
      class="${this.vueTagName}"
      v-bind:class="{ show:'${this.tagName}' === Router.coreAction }">
      <div>
        <button v-on:click="closeCore" class="close" title="Close">&times;</button>
        ${this.vueTemplateInner}
      </div>
    </div>
    `}

}
