//// Base class for all list modules, eg item/FileList.js.

//// A List is a module which maintains and renders an ordered collection of
//// items.




//// DEPENDENCIES

import Item from './item.js' // for itemClass, but also to record its API
import Row from './row.js' // for rowClass
import Module from './module.js'
import hub from '../core/hub.js'




//// CLASS

export default class List extends Module {




    //// VUE

    //// Add the 'oomod-list' class attribute to the template-string.
    get vueTemplate () { return `
        <div class="oomod-list ${this.vueTagName}">
          <div>${this.vueTemplateInner}</div>
        </div>`
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <${this.rowClass.vueTagName}
      v-for="item in ${this.constructor.name}.items"
      :key="item.id"
      v-bind:item="item">
    </${this.rowClass.vueTagName}>
    `}




    //// CONFIG

    //// Generally, the array of items should be persisted to localStorage.
    get persistList () { return [ 'items' ] }

    //// An instance of this class will be passed to 'state-add'. To be overridden!
    get itemClass () { return Item }

    //// Useful for building vueTemplate. To be overridden!
    get rowClass () { return Row }




    //// CONSTRUCTOR

    constructor () {
        super()
        const ns = this.constructor.name // namespace
        const itemTagName = this.itemClass.tagName

        hub.fire('state-set', ns, 'items', [])

        hub.on(itemTagName + '-add', () => {
            const id = hub.fire('state-add', ns, 'items', new this.itemClass() )
               .find( el => /^\d{7}$/.test(el) )
            if (! id) throw Error('Can’t find newly created item id')
            return id
        })

        hub.on(itemTagName + '-edit', (evtName, id, prop, val) =>
            hub.fire('state-edit', ns, 'items', id, prop, val ))

        hub.on(itemTagName + '-delete', (evtName, id) =>
            hub.fire('state-delete', ns, 'items', id ))
    }

}
