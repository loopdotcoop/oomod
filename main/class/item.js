//// Base class for all of the app’s items, eg item/File.js.




//// DEPENDENCIES

import config from '../core/config.js'
import hub from '../core/hub.js'
const IS_EDITABLE = 'IS_EDITABLE'
const IS_ON_FORM = 'IS_ON_FORM'
const IS_ON_ROW = 'IS_ON_ROW'




//// RECORD API

hub.on('all-modules-booted', e => {

    //// Create this item’s shared config object and record its `api`.
    if (! config.Item || ! config.Item.api)
        hub.fire('config-set', Item.name, 'api', Item.api)

})




//// CLASS

export default class Item {

    //// Xx, eg 'FooBar' becomes 'foo-bar'.
    static get tagName () { return '' +
        this.name.replace(/([a-z])([A-Z])/g,'$1-$2').toLowerCase() }




    //// API

    static get api () { return {
        id: {
            default: ~~(Math.random()*8999999+1000000) // six digits
          , IS_ON_FORM
        }
      , title: {
            default: `Untitled ${this.name}`
          , IS_EDITABLE, IS_ON_FORM, IS_ON_ROW
        }
      , tagName: this.tagName
      , className: this.name
      , isInvalid: false // generally, newly created things are valid
    } }




    //// CONSTRUCTOR

    constructor (custom={}) {

        //// Record custom instantiation values, or fall back to defaults.
        const { id, title } = custom
        const api = this.constructor.api
        this.id = null == id ? api.id.default : id
        this.title = title || api.title.default + ' #' + this.id

        //// tagName must be recorded like this, not as a `get tagName () { }`,
        //// so that Vue can see it when this item is recorded in state. Same
        //// goes for className.
        this.tagName = api.tagName
        this.className = api.className

        ////
        this.isInvalid = api.isInvalid
    }

}
